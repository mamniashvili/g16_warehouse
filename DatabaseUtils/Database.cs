﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseUtils {
    public class Database {

        public string ConnectionString { get; internal set; }

        public Database(string connectionString) => ConnectionString = connectionString;

        public Database() => ConnectionString = ConfigurationManager.ConnectionStrings[this.GetType().Name].ConnectionString ?? throw new Exception("Connection string not found!");

        public SqlConnection GetConnection(string connectionString) => new SqlConnection(connectionString);

        public SqlConnection GetConnection() => GetConnection(ConnectionString);

        public SqlCommand GetCommand(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters) {
            var command = new SqlCommand() {
                Connection = GetConnection(connectionString),
                CommandText = commandText,
                CommandType = commandType
            };

            foreach (var p in parameters) {
                command.Parameters.Add(p);
            }
            return command;
        }

        public SqlCommand GetCommand(string connectionString, string commandText, params SqlParameter[] parameters) => GetCommand(connectionString, commandText, CommandType.Text, parameters);

        public SqlCommand GetCommand(string commandText, CommandType commandType, params SqlParameter[] parameters) => GetCommand(ConnectionString, commandText, commandType, parameters);

        public SqlCommand GetCommand(string commandText, params SqlParameter[] parameters) => GetCommand(ConnectionString, commandText, CommandType.Text, parameters);

        public int ExecuteNonQuery(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters) {
            var command = GetCommand(connectionString, commandText, commandType, parameters);
            using (command.Connection) {
                command.Connection.Open();
                return command.ExecuteNonQuery();
            }
        }

        public int ExecuteNonQuery(string connectionString, string commandText, params SqlParameter[] parameters) => ExecuteNonQuery(connectionString, commandText, CommandType.Text, parameters);

        public int ExecuteNonQuery(string commandText, CommandType commandType, params SqlParameter[] parameters) => ExecuteNonQuery(ConnectionString, commandText, commandType, parameters);

        public int ExecuteNonQuery(string commandText, params SqlParameter[] parameters) => ExecuteNonQuery(ConnectionString, commandText, CommandType.Text, parameters);

        public object ExecuteScalar(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters) {
            var command = GetCommand(connectionString, commandText, commandType, parameters);
            using (command.Connection) {
                command.Connection.Open();
                return command.ExecuteScalar();
            }
        }

        public object ExecuteScalar(string connectionString, string commandText, params SqlParameter[] parameters) => ExecuteScalar(connectionString, commandText, CommandType.Text, parameters);

        public object ExecuteScalar(string commandText, CommandType commandType, params SqlParameter[] parameters) => ExecuteScalar(ConnectionString, commandText, commandType, parameters);

        public object ExecuteScalar(string commandText, params SqlParameter[] parameters) => ExecuteScalar(ConnectionString, commandText, CommandType.Text, parameters);

        public SqlDataReader ExecuteReader(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters) {
            var command = GetCommand(connectionString, commandText, commandType, parameters);
            command.Connection.Open();
            var reader = command.ExecuteReader();
            return reader;
        }

        public SqlDataReader ExecuteReader(string connectionString, string commandText, params SqlParameter[] parameters) => ExecuteReader(connectionString, commandText, CommandType.Text, parameters);

        public SqlDataReader ExecuteReader(string commandText, CommandType commandType, params SqlParameter[] parameters) => ExecuteReader(ConnectionString, commandText, commandType, parameters);

        public SqlDataReader ExecuteReader(string commandText, params SqlParameter[] parameters) => ExecuteReader(ConnectionString, commandText, CommandType.Text, parameters);

        public DataTable GetTable(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters) {
            var table = new DataTable();
            using (var reader = ExecuteReader(connectionString, commandText, commandType, parameters)) {
                table.Load(reader);
                return table;
            }
        }

        public DataTable GetTable(string connectionString, string commandText, params SqlParameter[] parameters) => GetTable(ConnectionString, commandText, CommandType.Text, parameters);

        public DataTable GetTable(string commandText, CommandType commandType, params SqlParameter[] parameters) => GetTable(ConnectionString, commandText, commandType, parameters);

        public DataTable GetTable(string commandText, params SqlParameter[] parameters) => GetTable(ConnectionString, commandText, CommandType.Text, parameters);
    }
}
