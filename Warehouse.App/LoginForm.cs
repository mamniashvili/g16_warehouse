﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Warehouse.Service;

namespace Warehouse.App {
	public partial class LoginForm : Form {
		public LoginForm() {
			InitializeComponent();
		}

        private void btnCancel_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
        }

        private void btnLogin_Click(object sender, EventArgs e) {
            try {
                if (new UserService().Login(txtUserName.Text, txtPassword.Text)) {
                    DialogResult = DialogResult.OK;
                } else {
                    MessageBox.Show("incorrect  User or Password.");
                }
                
            } catch (Exception ex) {
                MessageBox.Show(Convert.ToString(ex));
            }            
        }        
    }
}
