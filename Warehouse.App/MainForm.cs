﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warehouse.App {
	public partial class MainForm : Form {
		public MainForm() {
			InitializeComponent();
		}        

		private void toolStripButton1_Click(object sender, EventArgs e) {
			//ActiveMdiChild.Text = "Hello";
			foreach (ToolStripMenuItem menuItem in menuStrip1.Items) {
				menuItem.Text += "+";
				IterateMenuItems(menuItem);
			}
		}

		private void IterateMenuItems(ToolStripMenuItem item) {
			foreach (object stripMenuItem in item.DropDownItems) {
				if (!(stripMenuItem is ToolStripMenuItem)) {
					continue;
				}
				(stripMenuItem as ToolStripMenuItem).Text += "+";
				IterateMenuItems(stripMenuItem as ToolStripMenuItem);
			}
		}

		private void usersToolStripMenuItem_Click(object sender, EventArgs e) {
			UsersList usersList = new UsersList();
			usersList.MdiParent = this;
			usersList.Show();
		}
	}
}
