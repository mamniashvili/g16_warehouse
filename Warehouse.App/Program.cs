﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warehouse.App {
	static class Program {
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			//Application.Run(new LoginForm());

            var loginForm = new LoginForm();
            var result = loginForm.ShowDialog();

            if (result == DialogResult.OK) {
                Application.Run(new MainForm());
            } else if (result == DialogResult.OK) {
                loginForm.Close();
            }
        }
	}
}
