﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warehouse.App {
	public partial class UserDetails : Form {
		public int RecordID { get; private set; }
		public bool IsInEditMode => RecordID != 0;

		public UserDetails(int recordID = 0) {
			InitializeComponent();
			RecordID = recordID;
		}
	}
}
