﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain {

    public class Companies {
        public int ID { get; set; }
        public string Name { get; set; }
        public int IsDisabled { get; set; }
    }
}