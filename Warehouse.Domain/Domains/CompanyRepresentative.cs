﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain {

    public class CompanyRepresentative {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}