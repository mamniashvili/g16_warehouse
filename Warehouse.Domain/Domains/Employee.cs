﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain {

    public class Employee {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string IDNumber { get; set; }
        public decimal Salary { get; set; }
        public DateTime Birthday { get; set; }
        public bool IsDisabled { get; set; }
    }
}