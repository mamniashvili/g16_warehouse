﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain {

    public class GroupPermission {
        public int GroupID { get; set; }
        public int PermissionID { get; set; }
    }
}