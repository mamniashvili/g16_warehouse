﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain {

    public class Order {
        public int ID { get; set; }
        public DateTime OrderDate { get; set; }
        public int CompanyID { get; set; }
        public int EmployeeID { get; set; }
    }
}