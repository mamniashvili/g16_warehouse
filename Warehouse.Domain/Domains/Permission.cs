﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain {

    public class Permission {
        public int ID { get; set; }
        public string Name { get; set; }
        public byte Type { get; set; }
    }
}
