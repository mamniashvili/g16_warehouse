﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain {

    public class User {
        public int ID { get; set; }
        public string Username { get; set; }
        public byte[] Password { get; set; }
        public int IsDisabled { get; set; }
    }
}
