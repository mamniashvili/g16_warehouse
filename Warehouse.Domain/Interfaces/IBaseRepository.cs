﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain.Interfaces {

	public interface IBaseRepository {
		int Insert(params SqlParameter[] sqlParameters);
		int Update(params SqlParameter[] sqlParameters);
		int Delete(params SqlParameter[] sqlParameters);

		DataRow Get(params SqlParameter[] sqlParameters);
		DataTable Load();

		DataTable GetParameters(string procedureName, string procedure);
		DataTable GetParameters(SqlParameter sqlParameters, string procedure);
	}
}