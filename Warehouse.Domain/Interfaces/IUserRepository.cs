﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain.Interfaces {

	public interface IUserRepository : IBaseRepository {
		bool Login(string username, string password);
	}
}