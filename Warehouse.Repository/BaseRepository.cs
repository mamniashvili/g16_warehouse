﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DatabaseUtils;
using Warehouse.Domain.Interfaces;

namespace Warehouse.Repository {

    public abstract class BaseRepository : IBaseRepository {

        protected readonly Database _database;

        public BaseRepository() => _database = new Database();

        protected string ObjectName => GetType().Name.Replace("Repository", string.Empty) + "s";
        protected string InsertProcedureName => $"Insert{ObjectName}_SP";
        protected string UpdateProcedureName => $"Update{ObjectName}_SP";
        protected string DeleteProcedureName => $"Delete{ObjectName}_SP";
        protected string GetProcedureName => $"Get{ObjectName}_SP";

        public virtual int Insert(params SqlParameter[] sqlParameters) {
            return _database.ExecuteNonQuery(
                            InsertProcedureName,
                            CommandType.StoredProcedure,
                            sqlParameters
            );
        }

        public virtual int Update(params SqlParameter[] sqlParameters) {
            return _database.ExecuteNonQuery(
                            UpdateProcedureName,
                            CommandType.StoredProcedure,
                            sqlParameters
            );
        }

        public virtual int Delete(params SqlParameter[] sqlParameters) {
            return _database.ExecuteNonQuery(
                            DeleteProcedureName,
                            CommandType.StoredProcedure,
                            sqlParameters
            );
        }

        public DataRow Get(params SqlParameter[] sqlParameters) {
            return _database.GetTable(
                            GetProcedureName,
                            CommandType.StoredProcedure,
                            sqlParameters
            ).Rows[0];
        }

        public DataTable GetParameters(string procedureName, string procedure = "GetParameter_SP") {
            return _database.GetTable(
                            procedure,
                            CommandType.StoredProcedure,
                            new SqlParameter("@Name", SqlDbType.NVarChar) {
                                Value = procedureName
                            }
            );
        }

        public DataTable GetParameters(SqlParameter sqlParameters, string procedure = "GetParameter_SP") {
            return _database.GetTable(
                            procedure,
                            CommandType.StoredProcedure,
                            sqlParameters
            );
        }

        public DataTable Load() {
            return _database.GetTable(
                $@"Select * From { ObjectName }",
                CommandType.Text);
        }
    }
}