﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Domain.Interfaces;

namespace Warehouse.Repository {
	public class UserRepository : BaseRepository, IUserRepository {

        public bool Login(string username, string password) {
            var result = new SqlParameter("@result", SqlDbType.Bit) { Direction = ParameterDirection.ReturnValue };
            _database.ExecuteNonQuery(
                        "LogInUser",
                        CommandType.StoredProcedure,
                        new SqlParameter("@UserID", username),
                        new SqlParameter("@Password", password),
                        result
                        );
            return result.Value != DBNull.Value && Convert.ToBoolean(result.Value);
        }
    }
}