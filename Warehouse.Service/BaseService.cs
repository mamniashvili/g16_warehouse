﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Service {
    using Repository;
    using System.Data;
    using System.Data.SqlClient;
    using System.Reflection;

    public abstract class BaseService<TRecord, TRepository>
        where TRecord : class, new()
        where TRepository : BaseRepository, new() {

        protected readonly TRepository _repository;

        protected string ObjectName => typeof(TRecord).Name + "s";
        protected string InsertProcedureName => $"Insert{ObjectName}_SP";
        protected string UpdateProcedureName => $"Update{ObjectName}_SP";
        protected string DeleteProcedureName => $"Delete{ObjectName}_SP";
        protected string GetProcedureName => $"Get{ObjectName}_SP";

        public BaseService() => _repository = new TRepository();

        public TRecord Get(object id, string primaryKeyName = "ID") {
            DataRow dataRecord = _repository.Get(new SqlParameter(primaryKeyName, id));
            TRecord record = new TRecord();
            Type type = record.GetType();
            PropertyInfo[] recordProperties = type.GetProperties();

            foreach (var p in recordProperties) {
                type.GetProperty(p.Name).SetValue(record, dataRecord[p.Name]);
            }
            return record;
        }

        public SqlParameter[] SqlParameters(TRecord record, string procedureName) {
            DataTable table = _repository.GetParameters(new SqlParameter("@Name", procedureName));
            int count = table.Rows.Count;

            Type type = record.GetType();
            SqlParameter[] sqlParameter = new SqlParameter[count];
            PropertyInfo[] recordProperties = type.GetProperties();

            for (int i = 0; i < count; i++) {
                string columName = Convert.ToString(table.Rows[i][0]).Replace("@", string.Empty);
                object obj = type.GetProperty(columName).GetValue(record);
                sqlParameter[i] = new SqlParameter(columName, obj);
            }
            return sqlParameter;
        }

        public IEnumerable<TRecord> Load(Func<TRecord, bool> predicate) {
            Type type = new TRecord().GetType();
            PropertyInfo[] recordProperties = type.GetProperties();
            var table = _repository.Load();

            foreach (DataRow row in table.Rows) {
                TRecord record = new TRecord();
                foreach (var p in recordProperties) {
                    type.GetProperty(p.Name).SetValue(record, row[p.Name]);
                }
                if (predicate(record)) {
                    yield return record;
                }
            }
        }

        public object Update(TRecord record) => _repository.Update(SqlParameters(record, UpdateProcedureName));

        public object Insert(TRecord record) => _repository.Insert(SqlParameters(record, InsertProcedureName));

        public object Delete(TRecord record) => _repository.Delete(SqlParameters(record, DeleteProcedureName));

        public object Delete(object id, string primaryKeyName = "ID") => _repository.Delete(new SqlParameter(primaryKeyName, id));
    }
}