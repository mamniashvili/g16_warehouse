﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Service {
	using Domain;
	using Repository;

	public class UserService : BaseService<User, UserRepository> {
        public bool Login(string username, string password) {
            return _repository.Login(username, password);
        }
    }
}