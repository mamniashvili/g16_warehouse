﻿using System;
using Warehouse.Repository;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseUtils;
using System.Data;
using Warehouse.Domain;
using System.Reflection;
using Warehouse.Service;
using System.Text;
using System.Collections.Generic;

namespace Warehouse.Tests {

    [TestClass]
    public class RepositoryTests : BaseTests {
        //private object objectType;

        [TestMethod]
        public void Insert() {
            int num = new UserRepository().Insert(
                    new SqlParameter("ID", 6),
                    new SqlParameter("Username", "Givi"),
                    new SqlParameter("Password", "ae7b55c81")
            );
            Assert.AreEqual(num, 1, "Subtruction does not work.");
        }

        [TestMethod]
        public void Update() {
            int num = new UserRepository().Update(
                    new SqlParameter("ID", 6),
                    new SqlParameter("Username", "Gela"),
                    new SqlParameter("Password", "ae7b55c81"),
                    new SqlParameter("IsDisabled", "0")
            );
            Assert.AreEqual(num, 1, "Subtruction does not work.");
        }

        [TestMethod]
        public void Get() {
            DataRow row = new UserRepository().Get(new SqlParameter("ID", 6));

            Console.WriteLine(row[0]);
            Console.WriteLine(row[1]);
            Console.WriteLine(row[2]);
            Console.WriteLine(row[3]);
        }

        [TestMethod]
        public void Delete() {
            int num = new UserRepository().Delete(new SqlParameter("ID", 6));
            Assert.AreEqual(num, 1, "Subtruction does not work.");
        }

        [TestMethod]
        public void ServiceInsert() {
            User user = new User {
                ID = 1,
                Password = Encoding.ASCII.GetBytes("123456789"),
                Username = "Nika"
            };

            object obj = new UserService().Insert(user);

            Console.WriteLine(obj);
        }

        [TestMethod]
        public void ServiceUpdate() {
            User user = new User {
                ID = 1,
                Password = Encoding.ASCII.GetBytes("123456789"),
                Username = "Tera",
                IsDisabled = 1
            };
            object obj = new UserService().Update(user);
            Console.WriteLine(obj);
        }

        [TestMethod]
        public void ServiceGet() {
            User user = new UserService().Get(1);

            Console.WriteLine(user.ID);
            Console.WriteLine(user.Username);
            Console.WriteLine(user.Password);
            Console.WriteLine(user.IsDisabled);
        }

        [TestMethod]
        public void ServiceDelete() {
            var user = new User() { ID = 1 };

            object obj = new UserService().Delete(user);
            Console.WriteLine(obj);
        }

        [TestMethod]
        public void CompanyRepresentativeInsert() {
            CompanyRepresentative companyRepresentative = new CompanyRepresentative() {
                SocialSecurityNumber = "01018523647",
                FullName = "Kako Skvancheli",
                Email = "Kako.Skvancheli@gmail.com",
                Phone = "591013013",
                ID = 4
            };
            object obj = new CompanyRepresentativeService().Insert(companyRepresentative);
        }

        [TestMethod]
        public void CompanyRepresentativeDelete() {
            CompanyRepresentative companyRepresentative = new CompanyRepresentative() {
                SocialSecurityNumber = "01018523647",
                FullName = "Kako Skvancheli",
                Email = "Kako.Skvancheli@gmail.com",
                Phone = "591013013",
                ID = 4
            };
            object obj = new CompanyRepresentativeService().Delete(companyRepresentative);
        }

        [TestMethod]
        public void EmployeeInsert() {
            Employee employee = new Employee() {
                FirstName = "Kako",
                LastName = "Skvancheli",
                Birthday = new DateTime(1998, 7, 24),
                Email = "Kako.Skvancheli@gmail.com",
                IDNumber = "01018523647",
                IsDisabled = false,
                Salary = 1500
            };
            object obj = new EmployeeService().Insert(employee);
        }

        [TestMethod]
        public void Load() {
            IEnumerable<Employee> users = new EmployeeService().Load(r => r.FirstName == "Kako");

            foreach (var item in users) {
                Console.WriteLine(item.ID);
                Console.WriteLine(item.FirstName);
                Console.WriteLine(item.LastName);
                Console.WriteLine(item.Email);
                Console.WriteLine(item.IDNumber);
                Console.WriteLine(item.Birthday);
                Console.WriteLine(item.Salary);
                Console.WriteLine(item.IsDisabled);
                Console.WriteLine("__________________________________________________");
            }
        }
    }
}